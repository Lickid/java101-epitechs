package net.epitech.java.td05.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the SmartCardType database table.
 * 
 */
@Entity
@NamedQuery(name="SmartCardType.findAll", query="SELECT s FROM SmartCardType s")
public class SmartCardType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String typeName;

	//bi-directional many-to-one association to SmartCard
	@OneToMany(mappedBy="smartCardType")
	private List<SmartCard> smartCards;

	public SmartCardType() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTypeName() {
		return this.typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public List<SmartCard> getSmartCards() {
		return this.smartCards;
	}

	public void setSmartCards(List<SmartCard> smartCards) {
		this.smartCards = smartCards;
	}

	public SmartCard addSmartCard(SmartCard smartCard) {
		getSmartCards().add(smartCard);
		smartCard.setSmartCardType(this);

		return smartCard;
	}

	public SmartCard removeSmartCard(SmartCard smartCard) {
		getSmartCards().remove(smartCard);
		smartCard.setSmartCardType(null);

		return smartCard;
	}

}