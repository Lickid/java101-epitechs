package net.epitech.java.td03.dao.impl;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.epitech.java.td03.annotation.DBTable;
import net.epitech.java.td03.dao.AbstractSmartCardDAO;
import net.epitech.java.td03.exception.SmartCardException;
import net.epitech.java.td03.model.SmartCard;
import net.epitech.java.td03.model.SmartCardType;

import org.apache.commons.dbutils.DbUtils;

public class ReflexivitySmartCardDAO extends AbstractSmartCardDAO {

	@Override
	public void save(SmartCard card) {
		// TODO Auto-generated method stub

	}

	@Override
	public Collection<SmartCard> getSmartCardByType(SmartCardType type)
			throws SmartCardException {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement stmt = null;
		String dbName = null;

		DBTable dbtableAnnotation = SmartCard.class
				.getAnnotation(DBTable.class);
		if (dbtableAnnotation == null) {
			throw new SmartCardException(new Exception(
					"Entity Should be annotated with @DBTable"));
		}
		dbName = dbtableAnnotation.name();

		try {
			conn = this.datasource.getConnection();

			stmt = conn.prepareStatement("SELECT * FROM " + dbName
					+ " where type=?");
			stmt.setInt(1, type.getId());

			rs = stmt.executeQuery();

			List<SmartCard> cards = new ArrayList<SmartCard>();
			while (rs.next()) {
				ResultSetMetaData metadata = rs.getMetaData();
				SmartCard card = new SmartCard();
				for (int i = 1; i <= metadata.getColumnCount(); i++) {
					String columnName = metadata.getColumnName(i);

					Field field = card.getClass().getDeclaredField(columnName);
					field.setAccessible(true);
					field.set(card, rs.getObject(i));

				}
				cards.add(card);

			}

			return cards;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (NoSuchFieldException e) {
			throw new RuntimeException(e);
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (IllegalArgumentException e) {
			throw new SmartCardException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} finally {

			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(conn);
		}

	}

	@Override
	public Collection<SmartCard> getSmartCardByNmae(String holderName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(SmartCard card) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub

	}

}
