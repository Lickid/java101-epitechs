package net.epitech.java.td03.model;

import javax.annotation.Generated;

import net.epitech.java.td03.annotation.DBTable;

/**
 * SmartCard is a Querydsl bean type
 */
@Generated("com.mysema.query.codegen.BeanSerializer")
@DBTable(name="SmartCard")
public class SmartCard {

	
	private String holderName;

	private Integer id;

	private Integer type;

	public String getHolderName() {
		return holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

}
